package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import "sync"
import (
    "labrpc"
    "time"
    "math/rand"
    "bytes"
    "encoding/gob"
)

const(
    Follower = iota
    Candidate
    Leader
)

const(
    MinElectionTimeout = 400
    MaxElectionTimeout = 800
    HeartbeatTimeout = 150
)

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make().
//
type ApplyMsg struct {
    Index       int
    Command     interface{}
    UseSnapshot bool   // ignore for lab2; only used in lab3
    Snapshot    []byte // ignore for lab2; only used in lab3
}

type LogEntry struct {
    Command interface{}
    Term    int
}

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
    mu          sync.Mutex
    peers       []*labrpc.ClientEnd
    persister   *Persister
    me          int // index into peers[]
    role            int

    applyChan chan ApplyMsg
    commitIndexChangeChan chan int

    // Persistent state
    currentTerm int
    votedFor    map[int]int
    log         []LogEntry

    // Volatile state on all servers
    commitIndex int
    lastApplied int

    // Volatile state on leaders
    nextIndex   []int
    matchIndex  []int

    // Timers
    electionTimer   *time.Timer
    heartbeatTimer  *time.Timer
    resetElectionTimerChan chan bool
    resetHeartbeatTimerChan chan bool
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    var term int
    var isLeader bool
    term = rf.currentTerm
    isLeader = rf.role == Leader
    return term, isLeader
}

func (rf *Raft) currentRole() int {
    rf.mu.Lock()
    defer rf.mu.Unlock()
    return rf.role
}

func (rf *Raft) safeCurrentTerm() int {
    rf.mu.Lock()
    defer rf.mu.Unlock()
    return rf.currentTerm
}

//
// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
//
func (rf *Raft) persist() {
    w := new(bytes.Buffer)
    e := gob.NewEncoder(w)
    e.Encode(rf.currentTerm)
    e.Encode(rf.votedFor)
    e.Encode(rf.log)
    data := w.Bytes()
    rf.persister.SaveRaftState(data)
}

//
// restore previously persisted state.
//
func (rf *Raft) readPersist(data []byte) {
    r := bytes.NewBuffer(data)
    d := gob.NewDecoder(r)
    d.Decode(&rf.currentTerm)
    d.Decode(&rf.votedFor)
    d.Decode(&rf.log)
}


type RequestVoteArgs struct {
    Term         int
    CandidateId  int
    LastLogIndex int
    LastLogTerm  int
}

type RequestVoteReply struct {
    Term        int
    VoteGranted bool
}

type AppendEntriesArgs struct {
    Term int
    LeaderId int
    PrevLogIndex int
    PrevLogTerm int
    Entries []LogEntry
    LeaderCommit int
}

type AppendEntriesReply struct {
    Term int
    Success bool
    ConflictIndex int
}

func (rf *Raft) isLogMoreUpToDateThanMe(index int, term int) bool {
    myIndex, myTerm := rf.lastLogIndexAndTerm()
    return myTerm < term || (myTerm == term && myIndex <= index)
}

func (rf *Raft) RequestVote(args RequestVoteArgs, reply *RequestVoteReply) {
    rf.checkTerm(args.Term)

    currentTerm := rf.safeCurrentTerm()
    reply.Term = currentTerm
    reply.VoteGranted = false

    if args.Term < currentTerm {
        DPrintf("server %v rejected request vote for server %v because args.Term %v < currentTerm %v", rf.me, args.CandidateId, currentTerm, args.Term)
        return
    }

    candidateId, voted := rf.votedForTerm(args.Term)
    isLogMoreUpToDate := rf.isLogMoreUpToDateThanMe(args.LastLogIndex, args.LastLogTerm)
    DPrintf("server %v voted (%v) for %v", rf.me, voted, candidateId)
    if (!voted || candidateId == args.CandidateId) && isLogMoreUpToDate {
        rf.voteForTerm(args.Term, args.CandidateId)
        rf.backToFollower(args.Term)

        reply.Term = currentTerm
        reply.VoteGranted = true

        // vote granted, reset election timer
        rf.issueResetElectionTimer()

        DPrintf("server %v granted request vote for server %v", rf.me, args.CandidateId)
    } else {
        DPrintf("server %v rejected request vote for server %v because it voted (%v) for %v and log is up to date %v", rf.me, args.CandidateId, voted, candidateId, isLogMoreUpToDate)
    }
}

func (rf *Raft) sendRequestVote(server int, args RequestVoteArgs, reply *RequestVoteReply) {
    ok := false
    for !ok {
        ok = rf.peers[server].Call("Raft.RequestVote", args, reply)
    }
}

func (rf *Raft) AppendEntries(args AppendEntriesArgs, reply *AppendEntriesReply) {
    DPrintf("server %v got append entries, args %v", rf.me, args)

    reply.ConflictIndex = -1
    rf.checkTerm(args.Term)

    rf.mu.Lock()
    defer rf.mu.Unlock()

    // 1
    if args.Term < rf.currentTerm {
        reply.Term = rf.currentTerm
        reply.Success = false
        DPrintf("server %v return 1 args.Term %v rf.currentTerm %v\n", rf.me, args.Term, rf.currentTerm)
        return
    }

    // got append entries from current leader
    go rf.issueResetElectionTimer()

    // 2
    if args.PrevLogIndex > 0 {
        if args.PrevLogIndex >= len(rf.log) {
            reply.Term = rf.currentTerm
            reply.Success = false
            reply.ConflictIndex = len(rf.log)
            DPrintf("server %v return 2.1, logs %v\n", rf.me, rf.log)
            return
        } else if (rf.log[args.PrevLogIndex].Term != args.PrevLogTerm) {
            reply.Term = rf.currentTerm
            reply.Success = false

            conflictIndex := args.PrevLogIndex
            conflictTerm := rf.log[conflictIndex].Term
            for ; rf.log[conflictIndex].Term == conflictTerm; conflictIndex -- {
            }
            reply.ConflictIndex = conflictIndex + 1
            DPrintf("server %v return 2.2, conflict index %v logs %v\n", rf.me, reply.ConflictIndex, rf.log)
            return
        }
    }

    // 3
    oldLogIndex := 0
    newLogIndex := 0
    for newLogIndex = 0; newLogIndex < len(args.Entries); newLogIndex ++ {
        entry := args.Entries[newLogIndex]
        oldLogIndex = args.PrevLogIndex + 1 + newLogIndex
        if oldLogIndex < len(rf.log) {
            oldEntry := rf.log[oldLogIndex]
            if entry.Term != oldEntry.Term {
                rf.log = rf.log[:oldLogIndex]
                break
            }
        } else {
            break
        }
    }

    // 4
    if newLogIndex < len(args.Entries) {
        rf.log = append(rf.log, args.Entries[newLogIndex:]...)
    }

    rf.persist()

    DPrintf("server %v appending entries to log, result length %v, reuslt %v", rf.me, len(rf.log), rf.log)

    // 5
    if args.LeaderCommit > rf.commitIndex {
        if args.LeaderCommit > len(rf.log) {
            rf.commitIndex = len(rf.log)
        } else {
            rf.commitIndex = args.LeaderCommit
        }
    }

    rf.commitIndexChangeChan <- rf.commitIndex

    reply.Term = rf.currentTerm
    reply.Success = true
}

func (rf *Raft) sendAppendEntries(server int, args AppendEntriesArgs, reply *AppendEntriesReply) {
    ok := false
    for !ok {
        ok = rf.peers[server].Call("Raft.AppendEntries", args, reply)
    }
}

func (rf *Raft) appendLogEntry(entry LogEntry) int {
    index := len(rf.log)
    rf.log = append(rf.log, entry)
    return index
}


//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
    rf.mu.Lock()
    defer rf.mu.Unlock()
    index := -1

    term := rf.currentTerm
    isLeader := rf.role == Leader

    if isLeader {
        index = rf.appendLogEntry(LogEntry{command, term})
        rf.persist()
        go rf.issueResetHeartbeatTimer()
        go rf.appendEntriesToOtherServers()
    }

    return index, term, isLeader
}

//
// the tester calls Kill() when a Raft instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (rf *Raft) Kill() {
    // Your code here, if desired.
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
persister *Persister, applyCh chan ApplyMsg) *Raft {
    rand.Seed(time.Now().UTC().UnixNano())

    rf := &Raft{}
    rf.peers = peers
    rf.persister = persister
    rf.me = me

    rf.applyChan = applyCh
    rf.commitIndexChangeChan = make(chan int)

    // Your initialization code here.
    rf.role = Follower
    rf.currentTerm = 0
    rf.votedFor = make(map[int]int)
    rf.log = make([]LogEntry, 0)
    rf.log = append(rf.log, LogEntry{}) // In raft, log starts at index 1, so place a dummy log at index 0

    rf.commitIndex = 0
    rf.lastApplied = 0

    peerLen := len(rf.peers)
    rf.nextIndex = make([]int, peerLen)
    rf.matchIndex = make([]int, peerLen)
    for index := 0; index < peerLen; index ++  {
        rf.nextIndex[index] = 0
        rf.matchIndex[index] = 0
    }

    rf.resetElectionTimerChan = make(chan bool)
    rf.resetHeartbeatTimerChan = make(chan bool)

    // initialize from state persisted before a crash
    rf.readPersist(persister.ReadRaftState())

    // start election
    go rf.election()

    // start applier
    go rf.applyMsg()

    return rf
}

// RPC helpers
func (rf *Raft) checkTerm(term int) {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    DPrintf("server %v check term term %v currentTerm %v", rf.me, term, rf.currentTerm)
    if term > rf.currentTerm {
        rf.currentTerm = term
        rf.role = Follower
        rf.persist()
    }
}

func (rf *Raft) backToFollower(term int)  {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    rf.currentTerm = term
    rf.role = Follower

    rf.persist()
}


// Util funcs
func (rf *Raft) nextElectionTime() time.Duration {
    return time.Duration(rand.Intn((MaxElectionTimeout - MinElectionTimeout)) + MinElectionTimeout) * time.Millisecond
}

func (rf *Raft) initElectionTimer() {
    rf.electionTimer = time.NewTimer(rf.nextElectionTime())
}

func (rf *Raft) issueResetElectionTimer() {
    rf.resetElectionTimerChan <- true
}

func (rf *Raft) resetElectionTimer() {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    stop := rf.electionTimer.Stop()
    DPrintf("server %v election timer stopped %v", rf.me, stop)
    rf.electionTimer.Reset(rf.nextElectionTime())
}

func (rf *Raft) nextHeartbeatTime() time.Duration {
    return time.Duration(HeartbeatTimeout) * time.Millisecond
}

func (rf *Raft) initHeartbeatTimer()  {
    if rf.heartbeatTimer == nil {
        rf.heartbeatTimer = time.NewTimer(0) // start heartbeat immediately
    } else {
        rf.heartbeatTimer.Reset(0)
    }

}

func (rf *Raft) issueResetHeartbeatTimer() {
    rf.resetHeartbeatTimerChan <- true
}

func (rf *Raft) resetHeartbeatTimer() {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    stop := rf.heartbeatTimer.Stop()
    DPrintf("server %v heartbeat timer stopped %v", rf.me, stop)
    rf.heartbeatTimer.Reset(rf.nextHeartbeatTime())
}

func (rf *Raft) election() {
    rf.initElectionTimer()
    for {
        select {
        case <- rf.electionTimer.C:
            rf.becomeCandidate()
            go rf.issueResetElectionTimer()
            go rf.requestVoteFromOtherServers()
        case <- rf.resetElectionTimerChan:
            rf.resetElectionTimer()
        }
    }
}

func (rf *Raft) becomeCandidate() {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    rf.currentTerm ++
    rf.role = Candidate
    rf.votedFor[rf.currentTerm] = rf.me

    rf.persist()

    DPrintf("server %v became candidate", rf.me)
}

func (rf *Raft) votedForTerm(term int) (candidateId int, voted bool) {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    DPrintf("server %v votedFor %v", rf.me, rf.votedFor)

    val, ok := rf.votedFor[term]

    return val, ok
}

func (rf *Raft) voteForTerm(term int, server int)  {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    rf.votedFor[term] = server

    rf.persist()
}

func (rf *Raft) lastLogIndexAndTerm() (int, int) {
    index := len(rf.log) - 1
    entry := rf.log[index]
    term := entry.Term
    return index, term
}

func (rf *Raft) buildRequestVoteArgs() RequestVoteArgs {
    rf.mu.Lock()
    defer rf.mu.Unlock()
    args := RequestVoteArgs{}
    args.Term = rf.currentTerm
    args.CandidateId = rf.me
    args.LastLogIndex, args.LastLogTerm = rf.lastLogIndexAndTerm()
    return args
}

func (rf *Raft) requestVoteFromOtherServers () {
    replyChan := make(chan RequestVoteReply)
    quorum := len(rf.peers)
    waitReplyCount := quorum - 1
    args := rf.buildRequestVoteArgs()
    DPrintf("candidate %v before send request vote to other servers with args %v", rf.me, args)
    for server := range rf.peers {
        if server != rf.me {
            go func(server int) {
                reply := RequestVoteReply{}
                rf.sendRequestVote(server, args, &reply)
                rf.checkTerm(reply.Term)

                // if term not match, tell server to ignore it
                if rf.safeCurrentTerm() != args.Term {
                    reply.Term = -1
                    reply.VoteGranted = false
                }

                replyChan <- reply
            }(server)
        }
    }

    count := 0
    voteCount := 1 // 1 from myself
    for count < waitReplyCount {
        select {
        case reply := <- replyChan:
            count ++

            if reply.Term < 0 {
                continue
            }

            // only valid if i'm candidate
            role := rf.currentRole()
            if role == Candidate {
                if reply.VoteGranted {
                    voteCount ++
                }

                if voteCount > quorum / 2 {
                    rf.becomeLeader()
                    rf.heartbeat()
                }
            }
        }
    }
}

func (rf *Raft) becomeLeader() {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    nextIndex := len(rf.log)

    rf.role = Leader
    for server := range rf.peers {
        rf.nextIndex[server] = nextIndex
        rf.matchIndex[server] = 0
    }

    DPrintf("server %v became leader with term %v", rf.me, rf.currentTerm)
}

func (rf *Raft) heartbeat()  {
    rf.initHeartbeatTimer()
    for {
        select {
        case <- rf.heartbeatTimer.C:
            role := rf.currentRole()
            DPrintf("server %v in heartbeat with role %v", rf.me, role)
            if role == Leader {
                DPrintf("server %v before issue reset heartbeat timer", rf.me)
                go rf.issueResetHeartbeatTimer()
                DPrintf("server %v append entries to other servers", rf.me)
                go rf.appendEntriesToOtherServers()
            }
        case <- rf.resetHeartbeatTimerChan:
            DPrintf("server %v resetting heartbeat timer", rf.me)
            rf.resetHeartbeatTimer()
        }
    }
}

func (rf *Raft) buildAppendEntriesArgsForServer(server int) AppendEntriesArgs {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    args := AppendEntriesArgs{}
    args.Term = rf.currentTerm
    args.LeaderId = rf.me
    nextIndex := rf.nextIndex[server]
    DPrintf("server %v next index : %v, log length %v logs %v", server, nextIndex, len(rf.log), rf.log)
    args.PrevLogIndex = nextIndex - 1
    args.PrevLogTerm = rf.log[args.PrevLogIndex].Term
    newEntries := make([]LogEntry, len(rf.log))
    copy(newEntries, rf.log)
    newEntries = newEntries[nextIndex:]
    args.Entries = newEntries
    args.LeaderCommit = rf.commitIndex

    DPrintf("leader %v build entries for server %v with nextIndex %v entries %v", rf.me, server, nextIndex, args.Entries)
    return args
}

func (rf *Raft) updateMatchAndNextIndex(server int, matchIndex int)  {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    nextIndex := len(rf.log)

    DPrintf("server %v update matchIndex to %v", server, matchIndex)
    rf.matchIndex[server] = matchIndex
    rf.nextIndex[server] = nextIndex
}

func (rf *Raft) updateNextIndex(server int, nextIndex int)  {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    DPrintf("server %v update nextIndex to %v", server, nextIndex)

    if nextIndex <= 0 {
        nextIndex = len(rf.log)
    }

    rf.nextIndex[server] = nextIndex
}

func (rf *Raft) updateCommitIndex(index int) {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    toCommit := -1

    if index > rf.commitIndex && rf.log[index].Term == rf.currentTerm {
        toCommit = index
    }

    matchMap := make(map[int]int)
    for serverIndex := range rf.matchIndex {
        for i := index + 1; i <= serverIndex; i++ {
            matchMap[i] ++
        }
    }

    for k, v := range matchMap {
        DPrintf("WTF k %v v %v logs %v", k, v, rf.log)
        if v > len(rf.peers) / 2 && len(rf.log) > k && rf.log[k].Term == rf.currentTerm {
            toCommit = k
            break
        }
    }

    if toCommit > 0 {
        rf.commitIndex = index
        rf.commitIndexChangeChan <- rf.commitIndex
    }

    DPrintf("leader %v update commit index to %v", rf.me, rf.commitIndex)
}

func (rf *Raft) sendAppendEntriesToServer(server int, replyChan chan AppendEntriesReply)  {
    DPrintf("server %v sending append entries to server %v", rf.me, server)
    args := rf.buildAppendEntriesArgsForServer(server)
    reply := AppendEntriesReply{}
    shouldRetry := true

    for shouldRetry {
        rf.sendAppendEntries(server, args, &reply)
        DPrintf("leader %v got reply %v from server %v with args %v", rf.me, reply, server, args)

        rf.checkTerm(reply.Term)

        if rf.safeCurrentTerm() != args.Term {
            replyChan <- AppendEntriesReply{-1, false, -1}
            return
        }


        role := rf.currentRole()
        if role == Leader {
            if reply.Success {
                rf.updateMatchAndNextIndex(server, args.PrevLogIndex + len(args.Entries))
                shouldRetry = false

                replyChan <- reply
            } else {
                if reply.ConflictIndex < 0 {
                    shouldRetry = false
                    replyChan <- reply
                } else {
                    DPrintf("server %v dec nextIndex to %v for server %v", rf.me, reply.ConflictIndex, server)
                    rf.updateNextIndex(server, reply.ConflictIndex)
                    args = rf.buildAppendEntriesArgsForServer(server)
                    shouldRetry = true
                }
            }
        } else {
            shouldRetry = false
            replyChan <- AppendEntriesReply{-1, false, -1}
        }
    }
}

func (rf *Raft) appendEntriesToOtherServers () {
    replyChan := make(chan AppendEntriesReply)
    quorum := len(rf.peers)

    DPrintf("server %v trying to get last log index and term", rf.me)
    rf.mu.Lock()
    commitIndex, _ := rf.lastLogIndexAndTerm()
    rf.mu.Unlock()

    for server := range rf.peers {
        if server != rf.me {
            go rf.sendAppendEntriesToServer(server, replyChan)
        } else {
            go rf.resetElectionTimer()
        }
    }

    count := 1
    successCount := 1
    for count < quorum {
        select {
        case reply := <- replyChan:
            count ++
            if reply.Term < 0 {
                continue
            }
            DPrintf("getting message %v", reply)
            // only valid if i'm leader
            role := rf.currentRole()
            if role == Leader {
                if reply.Success {
                    successCount ++
                }

                DPrintf("success count %v quorum %v", successCount, quorum)

                if successCount > quorum / 2 {
                    rf.updateCommitIndex(commitIndex)
                }
            }
        }
    }
}

func (rf *Raft) logEntryAtIndex(index int) LogEntry {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    return rf.log[index]
}

func (rf *Raft) updateLastApplied(index int) {
    rf.mu.Lock()
    defer rf.mu.Unlock()

    if index > rf.lastApplied {
        rf.lastApplied = index
    }

}

func (rf *Raft) applyMsg() {
    for {
        select {
        case commitIndex := <- rf.commitIndexChangeChan:
            for entryIdx := rf.lastApplied + 1; entryIdx <= commitIndex; entryIdx ++ {
                DPrintf("server %v applying entry with index %v    step1", rf.me, entryIdx)
                logEntry := rf.log[entryIdx]
                DPrintf("server %v applying entry with index %v    step2", rf.me, entryIdx)
                msg := ApplyMsg {entryIdx, logEntry.Command, false, nil }
                rf.applyChan <- msg
                DPrintf("server %v applying entry with index %v    step3", rf.me, entryIdx)
                rf.lastApplied = entryIdx
            }
        }
    }
}