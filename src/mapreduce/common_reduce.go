package mapreduce

import (
	"os"
	"log"
	"encoding/json"
	"sort"
)

// doReduce does the job of a reduce worker: it reads the intermediate
// key/value pairs (produced by the map phase) for this task, sorts the
// intermediate key/value pairs by key, calls the user-defined reduce function
// (reduceF) for each key, and writes the output to disk.
func doReduce(
	jobName string, // the name of the whole MapReduce job
	reduceTaskNumber int, // which reduce task this is
	nMap int, // the number of map tasks that were run ("M" in the paper)
	reduceF func(key string, values []string) string,
) {
	cache := make(map[string][]string)
	for m := 0; m < nMap; m ++ {
		reduceFileName := reduceName(jobName, m, reduceTaskNumber)
		file, err := os.Open(reduceFileName)
		if err != nil {
			panic(err)
		}
		defer file.Close()

		dec := json.NewDecoder(file)
		for dec.More() {
			var kv KeyValue
			err := dec.Decode(&kv)
			if err != nil {
				panic(err)
			}

			if _, exists := cache[kv.Key]; !exists {
				cache[kv.Key] = make([]string, 0)
			}

			cache[kv.Key] = append(cache[kv.Key], kv.Value)
		}
	}
	
	mergeFileName := mergeName(jobName, reduceTaskNumber)
	wfile, err := os.Create(mergeFileName)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer wfile.Close()

	enc := json.NewEncoder(wfile)

	keys := make([]string, 0)
	for key := range cache {
		keys = append(keys, key)
	}

	sort.Strings(keys)
	for _, key := range keys {
		enc.Encode(KeyValue{key, reduceF(key, cache[key])})
	}
}
