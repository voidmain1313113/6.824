package mapreduce

import (
	"encoding/json"
	"hash/fnv"
	"os"
	"io/ioutil"
)

// doMap does the job of a map worker: it reads one of the input files
// (inFile), calls the user-defined map function (mapF) for that file's
// contents, and partitions the output into nReduce intermediate files.
func doMap(
	jobName string, // the name of the MapReduce job
	mapTaskNumber int, // which map task this is
	inFile string,
	nReduce int, // the number of reduce task that will be run ("R" in the paper)
	mapF func(file string, contents string) []KeyValue,
) {
	text, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	encoderMap := make(map[string]*json.Encoder)
	for _, kv := range mapF(inFile, string(text)) {
		key := kv.Key
		r := int(ihash(key) % uint32(nReduce))

		reduceFileName := reduceName(jobName, mapTaskNumber, r)
		enc := encoderMap[reduceFileName]
		if enc == nil {
			wfile, err := os.Create(reduceFileName)
			if err != nil {
				panic(err)
			}
			defer wfile.Close()

			enc = json.NewEncoder(wfile)
			encoderMap[reduceFileName] = enc
		}

		err := enc.Encode(&kv)
		if err != nil {
			panic(err)
		}
	}
}

func ihash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
