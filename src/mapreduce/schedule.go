package mapreduce

import "fmt"

// schedule starts and waits for all tasks in the given phase (Map or Reduce).
func (mr *Master) schedule(phase jobPhase) {
	var ntasks int
	var nios int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mr.files)
		nios = mr.nReduce
	case reducePhase:
		ntasks = mr.nReduce
		nios = len(mr.files)
	}

	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, nios)

	// All ntasks tasks have to be scheduled on workers, and only once all of
	// them have been completed successfully should the function return.
	// Remember that workers may fail, and that any given worker may finish
	// multiple tasks.
	//
	tasks := make(chan int, ntasks)
	tasksDone := make(chan int, ntasks)
	tasksRemaining := ntasks
	var worker string

	for task := 0; task < ntasks; task++ {
		tasks <- task
	}

	for {
		select {
		case task := <-tasks:
			worker = <-mr.registerChannel
			go func(task int, phase jobPhase, nios int, worker string) {
				ok := false
				switch phase {
				case mapPhase:
					ok = call(worker, "Worker.DoTask", &DoTaskArgs{mr.jobName, mr.files[task], phase, task, nios}, nil)
				case reducePhase:
					ok = call(worker, "Worker.DoTask", &DoTaskArgs{mr.jobName, "", phase, task, nios}, nil)
				}

				if !ok {
					tasks <- task
				} else {
					tasksDone <- 1
				}
				mr.registerChannel <- worker
			}(task, phase, nios, worker)
		case <-tasksDone:
			tasksRemaining = tasksRemaining - 1
			if tasksRemaining == 0 {
				fmt.Printf("Schedule: %v phase done\n", phase)
				return
			}
		}
	}
}
